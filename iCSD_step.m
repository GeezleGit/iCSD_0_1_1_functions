function [new_CSD_matrix,zs] = iCSD_step(ePot,el_pos,gauss_sigma,exCond,topCond,diam)
% [CSD,z] = iCSD_0_1_1('STEP', ... ,el_pos,gauss_sigma,exCond,topCond,diam)
% el_pos ........ electrode positions in mm
% gauss_sigma ... gaussian filter: std. dev.
% exCond ........ extracellular cortical conductivity parameter S/m
% topCond ....... topical (above cortex) cortical conductivity parameter S/m
% diam .......... % filter parameters:
gauss_sigma = gauss_sigma*1e-3;
filter_range = 5*gauss_sigma; % numeric filter must be finite in extent
if gauss_sigma<0
    errordlg('The gaussian filter width cannot be negative.')
    return
end;

% electrical parameters:
if exCond<=0
    errordlg('Ex. cond. has to be a positive number');
    return
end;

% size, potential (m1 has to equal number of electrode contacts)
[m1,m2] = size(ePot);

el_pos = el_pos.*1e-3; % mm -> m

% geometrical parameters:
diam = diam*1e-3; %diameter in [m]
if diam<=0
    errordlg('Diameter has to be a positive number.');
    return
end;

if topCond~=exCond & (el_pos~=abs(el_pos) | length(el_pos)~=length(nonzeros(el_pos)))
    errordlg('Electrode contact positions must be positive when top cond. is different from ex. cond.')
    return;
end;
if m1~=length(el_pos)
    errordlg(['Number of electrode contacts has to equal number of rows in potential matrix. Currently there are ',...
        num2str(length(el_pos)),' electrodes contacts, while the potential matrix has ',num2str(m1),' rows.']) 
    return
end;

% compute step iCSD:
CSD = F_const(el_pos,diam,exCond,topCond)^-1*ePot;

% make CSD continous (~200 points):
le = length(el_pos);
h = el_pos(2)-el_pos(1);
first_z = el_pos(1)-h/2; %plot starts at z1-h/2;
mfactor = ceil(200/le);
minizs = 0:h/mfactor:(mfactor-1 )*h/mfactor;
for i=1:size(CSD,1) % all rows
    zs((1:mfactor)+mfactor*(i-1)) = first_z+(i-1)*h+minizs;
    new_CSD_matrix((1:mfactor)+mfactor*(i-1),:) = repmat(CSD(i,:),mfactor,1);
end;


if gauss_sigma~=0 %filter iCSD
  [zs,new_CSD_matrix]=gaussian_filtering(zs,new_CSD_matrix,gauss_sigma,filter_range);
%  [new_positions,gfiltered_spline_CSD]=gaussian_filtering(zs,CSD_cs,gauss_sigma,filter_range);
end;
