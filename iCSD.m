function varargout = iCSD_0_1_1(CSDMode,ePot,varargin)

% implementation of iCSD toolbox to use as functions
%
% [...] = iCSD_0_1_1(CSDMode,ePot, ... )
%
% ePot .... evoked potential
%
% CSDMode
%
% CSD = iCSD_0_1_1('STANDARD', ... ,el_pos,b0,b1,cond,VakninFlag)
% el_pos ....... electrode positions in mm
% b0 ........... threepoint filter: center
% b1 ........... threepoint filter: neighbor
% exCond ....... extracellular cortical conductivity parameter S/m
% VakninFlag ... repeat first and last channel
%
% CSD = iCSD_0_1_1('DELTA', ... ,el_pos,b0,b1,exCond,topCond,diam)
% el_pos ....... electrode positions in mm
% b0 ........... threepoint filter: center
% b1 ........... threepoint filter: neighbor
% exCond ....... extracellular cortical conductivity parameter S/m
% topCond ...... topical (above cortex) cortical conductivity parameter S/m
% diam ......... 
%
% [CSD,z] = iCSD_0_1_1('STEP', ... ,el_pos,gauss_sigma,exCond,topCond,diam)
% el_pos ........ electrode positions in mm
% gauss_sigma ... gaussian filter: std. dev.
% exCond ........ extracellular cortical conductivity parameter S/m
% topCond ....... topical (above cortex) cortical conductivity parameter S/m
% diam .......... 
%
% [CSD,z] = iCSD_0_1_1('SPLINE', ... ,el_pos,gauss_sigma,exCond,topCond,diam)
% NOT IMPLEMENTED YET
%
%
% adapted from CSDplotter-0.1.1 by Klas H. Pettersen

switch upper(CSDMode)
    case 'STANDARD'
         CSD = iCSD_standard(ePot,varargin{:});
         varargout{1} = CSD;
    case 'DELTA'
        CSD = iCSD_delta(ePot,varargin{:});
         varargout{1} = CSD;
    case 'STEP'
        [CSD,zs] = iCSD_step(ePot,varargin{:});
         varargout{1} = CSD;
         varargout{2} = zs;
    case 'SPLINE'
        [CSD,zs] = iCSD_spline(ePot,varargin{:});
         varargout{1} = CSD;
         varargout{2} = zs;
    otherwise
        error('Don''t know option!');
%         CSD = standardCSD(ePot,el_pos,0,0,0.3,false);
%          varargout{1} = CSD;

end
