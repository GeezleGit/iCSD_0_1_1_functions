function [CSD,el_pos_plot] = iCSD_standard(ePot,el_pos,b0,b1,cond,VakninFlag)
% CSD = iCSD_0_1_1('STANDARD', ... ,el_pos,b0,b1,cond,VakninFlag)
% el_pos ....... electrode positions in mm
% b0 ........... threepoint filter: center
% b1 ........... threepoint filter: neighbor
% exCond ....... extracellular cortical conductivity parameter S/m
% VakninFlag ... repeat first and last channel
%
% cond ......... cortical parameter S/m
% b0 ........... threepoint filter: center
% b1 ........... threepoint filter: neighbor
% el_pos ....... electrode positions in mm
% VakninFlag ... repeat first and last channel

% filter parameters:
if b0+2*b1 == 0 && b1~=0
    errordlg('Singularity: b0+2*b1 cannot equal zero.');
    return
end;

% electrical parameters:
if cond<=0
    errordlg('ex. cond. has to be a positive number');
    return
end;

% size, potential (m1 has to equal number of electrode contacts)
[m1,m2] = size(ePot);

% electrode parameters:
el_pos = el_pos.*1e-3; % mm -> m
el_pos_plot = el_pos(2:length(el_pos)-1); % if not Vaknin electrodes
N = length(el_pos);
h = mean(diff(el_pos));
pot = ePot;
if m1~=N
    errordlg(['Number of electrode contacts has to equal number of rows in potential matrix. Currently there are ',...
        num2str(N),' electrodes contacts, while the potential matrix has ',num2str(m1),' rows.']) 
    return
end;

% compute standard CSD with vaknin el.
if VakninFlag
  el_pos_plot = el_pos;
  pot(1,:) = ePot(1,:);
  pot(2:m1+1,:)=ePot;
  pot(m1+2,:)=ePot(m1,:);
end;

CSD = -cond*D1(length(pot(:,1)),h)*pot;

if b1~=0 %filter iCSD (does not change size of CSD matrix)
  [n1,n2]=size(CSD);            
  CSD_add(1,:) = zeros(1,n2);   %add top and buttom row with zeros
  CSD_add(n1+2,:)=zeros(1,n2);
  CSD_add(2:n1+1,:)=CSD;        %CSD_add has n1+2 rows
  CSD = S_general(n1+2,b0,b1)*CSD_add; % CSD has n1 rows
end;