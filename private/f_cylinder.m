function out1 = f_cylinder(zeta,z,diam,cond)
  out1 = 1./(2.*cond).*(sqrt((diam/2)^2+((z-zeta)).^2)-abs(z-zeta));
return;