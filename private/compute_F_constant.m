function out = compute_F_constant(full_filename,el_pos,d,cond,cond_top,tol);
  N = length(el_pos);
  h = el_pos(2)-el_pos(1);
  out = zeros(N);        %define matrix
  for j = 1:N            %rows
    zj = el_pos(j);   %electrode positions
    for i = 1:N %columns
        if i~=1 %define lower integral limit
          lower_int = el_pos(i)-(el_pos(i)-el_pos(i-1))/2;
        else
          lower_int = max(0,el_pos(i)-h/2);
        end;
        if i~=N %define upper integral limit
          upper_int = el_pos(i)+(el_pos(i+1)-el_pos(i))/2;
        else
          upper_int = el_pos(i)+h/2;
        end;
  %      zi = el_pos(i);   %mid CSD position    
        out(j,i) = quad(@f_cylinder,lower_int,upper_int,tol,[],zj,d,cond) ...
            +(cond-cond_top)/(cond+cond_top) ...
            *quad(@f_cylinder,lower_int,upper_int,tol,[],-zj,d,cond);
    end; %for i
  end; %for j
  Fc = out;
  save(full_filename, 'Fc','tol');
return;