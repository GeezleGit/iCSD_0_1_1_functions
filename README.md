# iCSD_0_1_1_functions

Functions extract from the gui based toolbox of [Klas Pettersen](https://sites.google.com/site/klaspettersen/)

For the orginal toolbox and python implementations see https://github.com/espenhgn/CSDplotter

## Example

```matlab
eVP           = ones(nChans,nBins);% evoked potential

el_pos        = [0 0.150 0.300 ... ];% increasing depths in mm 
b0            = 1.00;
b1            = 0.00;
exCond        = 0.3;
PadFlag       = true;

[csd] = iCSD('STANDARD',eVP,el_pos,b0,b1,exCond,PadFlag);
```

## Units

```
CSD              = voltage/area          [V/m^2]
conductance      = current/voltage       [Siemens, S], [A/V]
conductivity     = conductance/distance
conductivity*CSD = current/volume

With
evoked potential in [V]
channel depths in [mm]
conductivity in [S/m]

we get nA/mm^3 or A/m^3

```
