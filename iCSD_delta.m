function [CSD] = iCSD_delta(ePot,el_pos,b0,b1,exCond,topCond,diam)
% CSD = iCSD_0_1_1('DELTA', ... ,el_pos,b0,b1,exCond,topCond,diam)
% el_pos ....... electrode positions in mm
% b0 ........... threepoint filter: center
% b1 ........... threepoint filter: neighbor
% exCond ....... extracellular cortical conductivity parameter S/m
% topCond ...... topical (above cortex) cortical conductivity parameter S/m
% diam ......... 
% exCond ......... cortical parameter S/m
% topCond .......
% b0 ........... threepoint filter: center
% b1 ........... threepoint filter: neighbor
% el_pos ....... electrode positions in mm
% diam ... repeat first and last channel

% filter parameters:
if b0+2*b1 == 0 && b1~=0
    errordlg('Singularity: b0+2*b1 cannot equal zero.');
    return
end;

% electrical parameters:
if exCond<=0
    errordlg('Ex. cond. has to be a positive number');
    return
end;

% size, potential (m1 has to equal number of electrode contacts)
[m1,m2] = size(ePot);

el_pos = el_pos.*1e-3; % mm -> m

% geometrical parameters:
diam = diam*1e-3; %diameter in [m]
if diam<=0
    errordlg('Diameter has to be a positive number.');
    return
end;

if topCond~=exCond & (el_pos~=abs(el_pos) | length(el_pos)~=length(nonzeros(el_pos)))
    errordlg('Electrode contact positions must be positive when top cond. is different from ex. cond.')
    return;
end;
if m1~=length(el_pos)
    errordlg(['Number of electrode contacts has to equal number of rows in potential matrix. Currently there are ',...
        num2str(length(el_pos)),' electrodes contacts, while the potential matrix has ',num2str(m1),' rows.']) 
    return
end;

% compute delta iCSD:
CSD = F_delta(el_pos,diam,exCond,topCond)^-1*ePot;

if b1~=0 %filter iCSD
  [n1,n2]=size(CSD);            
  CSD_add(1,:) = zeros(1,n2);   %add top and buttom row with zeros
  CSD_add(n1+2,:)=zeros(1,n2);
  CSD_add(2:n1+1,:)=CSD;        %CSD_add has n1+2 rows
  CSD = S_general(n1+2,b0,b1)*CSD_add; % CSD has n1 rows
end;
