function [CSD_cs,zs] = iCSD_spline(ePot,el_pos,gauss_sigma,exCond,topCond,diam)

% filter parameters:
gauss_sigma = gauss_sigma*1e-3; % mm -> m
filter_range = 5*gauss_sigma; % numeric filter must be finite in extent
if gauss_sigma<0
    errordlg('The gaussian filter width cannot be negative.')
    return
end;

% electrical parameters:
if exCond<=0
    errordlg('Ex. cond. has to be a positive number');
    return
end;

% size, potential (m1 has to equal number of electrode contacts)
[m1,m2] = size(ePot);

% geometrical parameters:
diam = diam*1e-3; %diameter in [m]
if diam<=0
    errordlg('Diameter has to be a positive number.');
    return
end;

el_pos = el_pos*1e-3;
if topCond~=exCond && (el_pos~=abs(el_pos) || length(el_pos)~=length(nonzeros(el_pos)))
    errordlg('Electrode contact positions must be positive when top cond. is different from ex. cond.')
    return;
end;
if m1~=length(el_pos)
    errordlg(['Number of electrode contacts has to equal number of rows in potential matrix. Currently there are ',...
        num2str(length(el_pos)),' electrodes contacts, while the potential matrix has ',num2str(m1),' rows.']) 
    return
end;

% compute spline iCSD:
Fcs = F_cubic_spline(el_pos,diam,exCond,topCond);
[zs,CSD_cs] = make_cubic_splines(el_pos,ePot,Fcs);
%[pos1,my_CSD_spline]=new_CSD_range(zs,CSD_cs,0,2.4e-3);
if gauss_sigma~=0 %filter iCSD
  [zs,CSD_cs]=gaussian_filtering(zs,CSD_cs,gauss_sigma,filter_range);
%  [new_positions,gfiltered_spline_CSD]=gaussian_filtering(zs,CSD_cs,gauss_sigma,filter_range);
end;
